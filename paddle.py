from turtle import Turtle
import time

class Paddle(Turtle):
    def __init__(self, user):
        super().__init__()
        self.hideturtle()
        self.up()
        self.color("white")
        self.shape("square")
        self.seth(90)
        self.shapesize(stretch_wid=1.0, stretch_len=5.0)
        self.user = user
        self.define_paddle()
        self.showturtle()
        # TODO: 1. Make dat shape longe

    # TODO: 2. Movement
    def define_paddle(self):
        if self.user == "player":
            self.goto(-350, 0)
        elif self.user == "cpu":
            self.goto(350, 0)

    def paddle_up(self):
        current_y = self.ycor()
        if current_y < 240:
            # self.sety(current_y + 20)
            self.fd(20)


    def paddle_down(self):
        current_y = self.ycor()
        if current_y > -240:
            # self.sety(current_y - 20)
            self.bk(20)