from turtle import Turtle, Screen
ALIGNMENT = "center"
FONT = ("Arial", 50, "normal")
GO = (0, 0)


class Scoreboard(Turtle):
    def __init__(self, user):
        super().__init__()
        self.score = 0
        self.color("white")
        self.up()
        self.user = user
        self.define_score()
        self.hideturtle()
        self.update_score()


    def add_score(self):
        self.score += 1


    def define_score(self):
        if self.user == 'player':
            self.goto(-30, 230)
        elif self.user == 'cpu':
            self.goto(30, 230)

    def update_score(self):
        self.clear()
        self.write(f"{self.score}", align=ALIGNMENT, font=FONT)