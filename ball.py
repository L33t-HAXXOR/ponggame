from turtle import Turtle



class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.up()
        self.hideturtle()
        self.shape("circle")
        self.color("white")
        self.serve_dir = [135, 45]
        self.seth(135)
        self.showturtle()


    def ricochet(self, head_set):
        current_heading = self.heading()
        self.seth(current_heading - head_set)

    def set_serve(self, dir):
        self.seth(self.serve_dir[dir])