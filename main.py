from turtle import Screen
import time
import random as r

# CLASS IMPORTS
from scoreboard import Scoreboard
from paddle import Paddle
from ball import Ball

# TODO 1. Write some game logic

screen = Screen()
screen.setup(810, 602)
screen.screensize(800, 600)
screen.bgcolor("black")
screen.title("Pong")
screen.tracer(0)

paddle_l = Paddle("player")
paddle_r = Paddle("cpu")
ball = Ball()
player = Scoreboard('player')
cpu = Scoreboard('cpu')

def gaem():
    screen.resetscreen()
    paddle_r.__init__("cpu")
    paddle_l.__init__("player")
    ball.__init__()
    player.__init__('player')
    cpu.__init__('cpu')

    game = True
    while game == True:
        print(ball.pos())
        time.sleep(0.1)
        screen.update()
        ball.fd(10)
        if ball.ycor() >= 280 or ball.ycor() <= -280:
            ball.ricochet(270)
        if ball.distance(paddle_r) < 19 or ball.distance(paddle_l) < 19:
            ball.ricochet(270)
        elif ball.xcor() > 330 and ball.distance(paddle_r) < 50:
            ball.ricochet(270)
        elif ball.xcor() < -330 and ball.distance(paddle_l) < 50:
            print("\n\n\n")
            ball.ricochet(270)
        elif ball.xcor() > 420:
            player.score += 1
            player.update_score()
            ball.set_serve(0)
            ball.goto(0, 0)
        elif ball.xcor() < -420:
            cpu.score += 1
            cpu.update_score()
            ball.set_serve(1)
            ball.goto(0, 0)

## Event Listeners
screen.listen()
screen.onkey(key="w", fun=paddle_l.paddle_up)
screen.onkey(key="s", fun=paddle_l.paddle_down)
screen.onkey(key="Up", fun=paddle_r.paddle_up)
screen.onkey(key="Down", fun=paddle_r.paddle_down)

# DEBUG
screen.onkey(key="r", fun=gaem)
screen.onkey(key="x", fun=screen.bye)


gaem()


screen.mainloop()